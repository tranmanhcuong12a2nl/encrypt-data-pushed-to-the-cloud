import pickle
import os.path
import io
from google.oauth2.credentials import Credentials
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from googleapiclient.http import MediaFileUpload, MediaIoBaseDownload
import platform
from getpass import getuser

parents_id = []

SCOPES = ['https://www.googleapis.com/auth/drive']

operating_systerm = platform.system()

# Default path for storing the keys, encrypted file and decrypted file.
# if operating_systerm == "Windows":
#     default_path = f"C:/Users/{getuser()}/Downloads"
# elif operating_systerm == "Linux":
#     default_path = f"home/{getuser()}/Downloads"
# else:
default_path = f"C:/Users/{getuser()}/Downloads"

def auth():
    creds = None
    
    if os.path.exists("token.json"):
        creds= Credentials.from_authorized_user_file("token.json",SCOPES)
    
    if not creds or not creds.valid:
        if creds and  creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow=InstalledAppFlow.from_client_secrets_file("credentials.json",SCOPES)
            creds=flow.run_local_server(port=0)
        with open('token.json','w') as token:
            token.write(creds.to_json())

    global service
    service = build("drive","v3",credentials=creds)
    
    
    # creds = None
    # if os.path.exists("token.json"):
    #     creds= Credentials.from_authorized_user_file("token.json",SCOPES)
    
    # if not creds or not creds.valid:
    #     if creds and  creds.expired and creds.refresh_token:
    #         creds.refresh(Request())
    #     else:
    #         flow=InstalledAppFlow.from_client_secrets_file("credentials.json",SCOPES)
    #         creds=flow.run_local_server(port=0)
    #     with open('token.json','w') as token:
    #         token.write(creds.to_json())
    
    # global service
    # service = build("drive","v3",credentials=creds)

def pull(data):
    pull2("{}".format(data), data)

    
def pull2(name,orgi):
    puller(10,"name contains '{}'".format(name), name,orgi)
    
def pull_id(file_id,file_name):
    puller_id(file_id,file_name)

  
def puller(size,query,name,orgi):
    auth()
    results = service.files().list(
    pageSize=size,fields="nextPageToken, files(id, name, kind, mimeType, size)",q=query).execute()
    items = results.get('files', [])
    print(items)
    if not items:
        print('No files found.')
    else:
        for item in items:
            print(item['name'],'%.2fMB' % (int(item['size'])/1048576))
        request = service.files().get_media(fileId=item['id'])
        fh = io.BytesIO()
        downloader = MediaIoBaseDownload(fh, request)
        done = False
        while done is False:
            status, done = downloader.next_chunk()
            print("Download %d%%." % int(status.progress() * 100))
        with io.open(item['name'], 'wb') as f:
            fh.seek(0)
            f.write(fh.read())
            print(name)
            # decrypt_driver.decrypt(name)
            

def puller_id(file_id, name):
    auth()
    result = service.files().get_media(fileId=file_id)
    fh = io.BytesIO()
    downloader = MediaIoBaseDownload(fh, result)
    done = False
    while done is False:
        status, done = downloader.next_chunk()
        print("Download %d%%." % int(status.progress() * 100))

    with io.open(name, 'wb') as f:
        fh.seek(0)
        f.write(fh.read())

    print("File downloaded:", name)
    
    

         
            
def pullerdir(data,namepath):
    idd,parr ='',namepath
    auth()
    page_token=None
    while True:
        response = service.files().list(q="'{}' in parents".format(data),
                                        spaces='drive',
                                        fields='nextPageToken, files(id, name, size,mimeType, modifiedTime)',
                                        pageToken=page_token).execute()
        for file in response.get('files', []):
            namepath = parr+'/'+file.get('name')
            if file.get('mimeType')=='application/vnd.google-apps.folder':
                print(parr)
                os.mkdir(namepath)
                pullerdir(file.get('id'), namepath)
            else:
                request = service.files().get_media(fileId=file.get('id'))
                fh = io.BytesIO()
                downloader = MediaIoBaseDownload(fh, request)
                done = False
                while done is False:
                    status, done = downloader.next_chunk()
                    print(namepath)
                    with io.open(namepath, 'wb') as f:
                        fh.seek(0)
                        f.write(fh.read())
        page_token = response.get('nextPageToken', None)
        if page_token is None:
            break



def pulldir(data):
    auth()
    response = service.files().list(q="name contains '{}'".format(data),
                                    spaces='drive',
                                    fields='nextPageToken, files(id)',
                                    ).execute()
    for file in response.get('files', []):
        os.mkdir(data)
        pullerdir(file.get('id'),data)
        # decrypt_driver.decryptdir(data)
        break

def pull_folder_by_id(folder_id, destination_folder):
    auth()
    os.makedirs(destination_folder, exist_ok=True)
    pullerdir(folder_id, destination_folder)
    msg = "Download successful"
    return msg
    
# pull_folder_by_id('1bhwEi3y0xlGU9k9LKvRbmd6rFyKxmasC','backupfile')