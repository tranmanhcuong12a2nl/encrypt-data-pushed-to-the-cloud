import os
import os.path

from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from googleapiclient.http import MediaFileUpload
from getpass import getuser
from hybrid_encryption import *
import pathlib
import platform
import folder_driver

parents_id = []
SCOPES=["https://www.googleapis.com/auth/drive"]
# creds=None

operating_systerm = platform.system()

# if operating_systerm == "Windows":
#     default_path = f"C:/Users/{getuser()}/Hybrid Encryption"
# elif operating_systerm == "Linux":
#     default_path = f"home/{getuser()}/Hybrid Encryption"
# else:
default_path = f"C:/Users/{getuser()}/Hybrid Encryption"
    
# default_path = f"C:/Users/{getuser()}/Hybrid Encryption"


def auth():
    creds = None
    
    if os.path.exists("token.json"):
        creds= Credentials.from_authorized_user_file("token.json",SCOPES)
    
    if not creds or not creds.valid:
        if creds and  creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow=InstalledAppFlow.from_client_secrets_file("credentials.json",SCOPES)
            creds=flow.run_local_server(port=0)
        with open('token.json','w') as token:
            token.write(creds.to_json())

    global service
    service = build("drive","v3",credentials=creds)
    
    
    # creds = None
    
    # if os.path.exists("token.json"):
    #     creds= Credentials.from_authorized_user_file("token.json",SCOPES)
    
    # if not creds or not creds.valid:
    #     if creds and  creds.expired and creds.refresh_token:
    #         creds.refresh(Request())
    #     else:
    #         flow=InstalledAppFlow.from_client_secrets_file("credentials.json",SCOPES)
    #         creds=flow.run_local_server(port=0)
    #     with open('token.json','w') as token:
    #         token.write(creds.to_json())
    
    # global service
    # service = build("drive","v3",credentials=creds)
    
    

def delete_all_file(dir):
    files = os.listdir(dir)
    
    for file in files:
        if file.endswith(".enc"):
            file_path = os.path.join(dir,file)
            os.remove(file_path)
    
    

def get_file_name(file_path, extension=False):
    """Returns file name from given path
    either with extension or without extension

    :param file_path:
    :param extension:
    :return file_name:
    """
    if not extension:
        file_name = pathlib.Path(file_path).stem

    else:
        file_name = os.path.basename(file_path)

    return file_name

def get_file_path(file_path):
    """Returns the file_path for storing the encrypted file"""

    file_name = os.path.basename(file_path)
    file_path = f"{default_path}/{file_name}.enc"
    return file_path

def pusher2(name, namepath,parents):
    file_metadata = {'name': name, 'parents': parents}
    media = MediaFileUpload(namepath,
                            mimetype='application/octet-stream')
    print(name, " is being uploaded")
    file = service.files().create(body=file_metadata,
                                  media_body=media,
                                  fields='id').execute()
    
    
def check():
    auth()
    page_token = None
    while True:
        response = service.files().list(q="name contains 'BackupFolder2023'",
                                        spaces='drive',
                                        fields='nextPageToken, files(id, name, mimeType)',
                                        pageToken=page_token).execute()
        for file in response.get('files', []):
            if 'application/vnd.google-apps.folder'==file.get('mimeType'):
                parents_id.append(file.get('id'))
                break
            else:
                continue
        page_token = response.get('nextPageToken', None)
        if page_token is None:
            if parents_id == []:
                folder_driver.mkdir("BackupFolder2023")
                check()
            break



def pushdirchild(dir,parent):
    a = os.path.split(dir)
    par = ['{}'.format(folder_driver.createFolder(a[-1], parent))]
    for item in os.listdir(dir):
        if os.path.isdir(dir+'/'+item):
            pushdirchild(dir+'/'+item,par)
        else:
            pusher2(item, "{}/{}".format(dir,item),par)

            
                  
def pushdir(dir):
    check()
    # encryptDir(dir)
    a = os.path.split(dir)
    par = ['{}'.format(folder_driver.createFolder(a[-1], parents_id))]
    for item in os.listdir(dir):
        if os.path.isdir(dir+'/'+item):
            pushdirchild(dir+'/'+item,par)
        else:
            pusher2(item, "{}/{}".format(dir,item),par)
    msg = "the upload process is success"
    return msg

    

def push(file_path):
    auth()
    # creds = None
    
    # if os.path.exists("token.json"):
    #     creds= Credentials.from_authorized_user_file("token.json",SCOPES)
    
    # if not creds or not creds.valid:
    #     if creds and  creds.expired and creds.refresh_token:
    #         creds.refresh(Request())
    #     else:
    #         flow=InstalledAppFlow.from_client_secrets_file("credentials.json",SCOPES)
    #         creds=flow.run_local_server(port=0)
    #     with open('token.json','w') as token:
    #         token.write(creds.to_json())

    try:
        # service = build("drive","v3",credentials=creds)
        response =service.files().list(q="name='BackupFolder2023'and mimeType='application/vnd.google-apps.folder'",
                                    spaces='drive').execute()
        
        if not response['files']:
            file_metadata={
                "name":"Backup",
                "mimeType":"application/vnd.google-apps.folder"
            }

            file =service.files().create(body=file_metadata, fields="id").execute()
            folder_id=file.get('id')
        else:
            folder_id=response['files'][0]['id']
            
        for file in os.listdir(default_path):
            if file.endswith('.enc'):
                file_metadata={
                    "name":file,
                    "parents":[folder_id]
                }
                media= MediaFileUpload("{}".format(f"{ default_path }/{ file }"))
                upload_file=service.files().create(body=file_metadata, media_body=media,fields="id").execute()
                print("Backed up file: "+ file)
                
        delete_all_file(default_path)
        
        

    except HttpError as e:
        print("Error:   "+ str(e))
        
        
# pushdir("backupfile")