import os
import difflib
import pickle

# history_file = ''

    
# path_components = os.path.normpath(".").split(os.sep)
# history_file = f'histories/{ path_components[-1] }.pkl'


def get_history_file(folder_path):
    path_components = os.path.normpath(folder_path).split(os.sep)
    history_file = f'histories/{ path_components[-1] }.pkl'
    
    return history_file

def get_previous_file(folder_path,history_file):
    
    if os.path.exists(history_file):
        with open(history_file, 'rb') as f:
            previous_files = pickle.load(f)  # Đọc danh sách các file từ file lịch sử trước đó
    else:
        previous_files = {}
    
    return previous_files

def get_current_file(folder_path):
    current_files = {}
    for root, dirs, files in os.walk(folder_path):
        for file in files:
            file_path = os.path.join(root, file)
            mtime = os.path.getmtime(file_path)  # Lấy thời gian sửa đổi của file
            current_files[file_path] = mtime
    
    return current_files

def compare_diff(previous_files, current_files):
    # previous_files = get_previous_file(folder_path)
    # current_files = get_current_file(folder_path)
    diff = difflib.ndiff(list(previous_files.keys()), list(current_files.keys()))
    
    return diff

def save_diff(history_file,current_files):
    with open(history_file, 'wb') as f:
        pickle.dump(current_files, f)   

def checking_diff(folder_path):
    
    history_file = get_history_file(folder_path)
    previous_files = get_previous_file(folder_path,history_file)
    current_files = get_current_file(folder_path)
    diff = difflib.ndiff(list(previous_files.keys()), list(current_files.keys()))
    
    for line in diff:
        
        if line.startswith('- '):
            file_path = line[2:]
            if file_path in previous_files:
                print(f"File removed: {file_path}")
        elif line.startswith('+ '):
            file_path = line[2:]
            if file_path in current_files:
                print(f"File added: {file_path}")
        else:
            file_path = line[2:]
            if file_path in previous_files and file_path in current_files:
                previous_mtime = previous_files[file_path] 
                current_mtime = current_files[file_path]
                if previous_mtime != current_mtime:
                    print(f"File modified: {file_path}")
                    
    save_diff(history_file,current_files)
    
    
    
# get_history_file(".")
checking_diff("/home/honahl/Documents/tempProject")