from __future__ import print_function
import pickle
import os.path
import io
from googleapiclient.discovery import build
from google.oauth2.credentials import Credentials
import re
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import os
from cryptography.hazmat.backends import default_backend



parents_id = []

SCOPES = ['https://www.googleapis.com/auth/drive']

def auth():
    creds = None
    
    if os.path.exists("token.json"):
        creds= Credentials.from_authorized_user_file("token.json",SCOPES)
    
    if not creds or not creds.valid:
        if creds and  creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow=InstalledAppFlow.from_client_secrets_file("credentials.json",SCOPES)
            creds=flow.run_local_server(port=0)
        with open('token.json','w') as token:
            token.write(creds.to_json())

    global service
    service = build("drive","v3",credentials=creds)
    # creds = None
    # if os.path.exists('token.pickle'):
    #     with open('token.pickle', 'rb') as token:
    #         creds = pickle.load(token)
    # if not creds or not creds.valid:
    #     if creds and creds.expired and creds.refresh_token:
    #         creds.refresh(Request())
    #     else:
    #         flow = InstalledAppFlow.from_client_secrets_file(
    #             'credentials.json', SCOPES)
    #         creds = flow.run_local_server(port=0)
    #     with open('token.pickle', 'wb') as token:
    #         pickle.dump(creds, token)
    # global service
    # service = build('drive', 'v3', credentials=creds)

def lookfor(query):
    auth()
    page_token = None
    while True:
        response = service.files().list(q="name contains '{}'".format(query),
                                        spaces='drive',
                                        fields='nextPageToken, files(id, name, size, modifiedTime)',
                                        pageToken=page_token).execute()
        for file in response.get('files', []):
            print('Found file: %s %s %s' % (file.get('name'),file.get('id'),file.get('modifiedTime')))
        page_token = response.get('nextPageToken', None)
        if page_token is None:
            break

def extract_folder_id():
    folder_url = "https://drive.google.com/drive/u/2/folders/1CQUPFBPBZNPjpf42kubHSOFinP7sHlRb"
    pattern = r'[-\w]{25,}'
    match = re.search(pattern, folder_url)
    if match:
        return match.group()
    else:
        raise ValueError("Invalid Google Drive folder URL")

# Example usage


# print("Folder ID:", folder_id)

def list_files_and_folders(folder_id):
    auth()
    
    files = []
    folders = []
    
    query = f"'{folder_id}' in parents and trashed=false"
    
    page_token = None
    while True:
        response = service.files().list(q=query,
                                        fields="nextPageToken, files(id, name, mimeType)",
                                        pageSize=1000,
                                        pageToken=page_token).execute()
        files.extend(response.get('files', []))
        page_token = response.get('nextPageToken', None)
        if page_token is None:
            break
    
    for file in files:
        if file['mimeType'] == 'application/vnd.google-apps.folder':
            folders.append(file)
    
    return files, folders

def print_files_and_folders(files, folders, indent=1):
    for file in files:
        print('-' * indent + file['name'], "-", file['id'])
    
    for folder in folders:
        print('+' * indent + folder['name'], "-", folder['id'])
        subfiles, subfolders = list_files_and_folders(folder['id'])
        print_files_and_folders(subfiles, subfolders, indent=indent + 2)

def mkdir(name):
    auth()
    file_metadata = {
    'name' : name,
    'mimeType' : 'application/vnd.google-apps.folder'
    }
    file = service.files().create(body=file_metadata,
                                        fields='id').execute()
    return file.get('id')


def createFolder(name,parents):
    auth()
    file_metadata = {
    'name': name,
    'mimeType': 'application/vnd.google-apps.folder', 'parents' : parents
    }
    file = service.files().create(body=file_metadata,
                                        fields='id').execute()
    return file.get('id')

folder_id = extract_folder_id()
print(folder_id)
files, folders = list_files_and_folders(folder_id)
print_files_and_folders(files, folders)

