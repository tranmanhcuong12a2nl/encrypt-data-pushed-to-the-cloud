from tkinter import *
from tkinter import ttk
from tkinter.filedialog import askopenfilename, askdirectory
from push_drive import *
from pull_driver import *
from folder_driver import *
from getpass import getuser
from ttkthemes import themed_tk as tk
from hybrid_encryption import *
from tkinter import messagebox
from tkinter import Toplevel
from datetime import datetime
upload_button = None
download_button = None
schedule_button = None
look_file_button = None
encrypt_button=None
decrypt_button=None
separator2=None
mgs_label=None
# selection=None
# selection_file=None
# selection_folder=None
# selection_name=None


def message(name, button, type ,mgs_label):
    
    if type == 'file':
    # button['state'] = DISABLED
        if button["text"] == "Encryption":
            mgs = encryption(name)
            mgs_label.config(text=mgs)

        if button["text"] == "Decryption":
            mgs = decryption(name)
            mgs_label.config(text=mgs)

        
        if button["text"] == "Upload":
            mgs = push(name)
            mgs_label.config(text=mgs)
            trv2.delete(*trv2.get_children())
            folder_id = extract_folder_id()
            files, folders = list_files_and_folders(folder_id)
            tree_files_and_folders(files,folders)
    # if button['text'] == 'Download':
    elif type == 'folder':
        if button['text'] == "Encryption":
            mgs = encryptDir(name)
            mgs_label.config(text=mgs)
            
        if button['text'] == 'Upload': # print(selection[1],selection[0])
                # mgs = pull_folder_by_id(selection[1],selection[0])
                # decryptdir(os.path.join(default_path, selection[0]))
                # mgs_label.config(text=mgs)
            encryptDir(name)
            folder_name = os.path.normpath(name).split(os.sep)
            push_dir = os.path.join(default_path,folder_name[-1])
            mgs = pushdir(push_dir)
            trv2.delete(*trv2.get_children())
            folder_id = extract_folder_id()
            files, folders = list_files_and_folders(folder_id)
            tree_files_and_folders(files,folders)
            mgs_label.config(text=mgs)
            
        if button['text'] == 'Decryption':
            mgs = decryptdir(name)
            my_fun_2(name)
            mgs_label.config(text=mgs)
        
    else:
        if button["text"] == "Download":
            if selection != None:
                if selection[2] == "application/vnd.google-apps.folder": 
                    print(selection[1],selection[0])
                    mgs = pull_folder_by_id(selection[1],selection[0])
                    decryptdir(selection[0])
                    mgs_label.config(text=mgs)
                else:
                    # decryption(name)
                    print(selection[1],selection[0])
                    mgs = pull_id(selection[1],selection[0])
                    mgs_label.config(text=mgs)
            else:
                mgs_label.config(text="select file you want to download")
        else:
            mgs_label.config(text="select folder or file you want to")
    
    if button["text"] == "Schedule Upload":
        show_schedule_entry(name, mgs_label)
    pass

def open_file():
    """
    This function opens the file dialog box for choosing a file.
    And then creates two buttons: encrypt_button, decrypt_button
    """
    username = getuser()
    initialdirectory = "C:/Users/{}".format(username)
    name = askopenfilename(
        initialdir=initialdirectory,
        filetypes=[("All Files", "*.*")],
        title="Choose a file."
    )
    if name:
        file_name = get_file_name(name, extension=True)
        label.config(text=file_name)
        mgs_label = ttk.Label(root)
        mgs_label.place(x=0, y=150)
        show_all_button(name,type='file',mgs_label=mgs_label)
        # encrypt_button = ttk.Button(root, text="Encryption", command=lambda: [reset_app()
        #                                                                       , show_additional_buttons(name,mgs_label)
        #                                                                       ,message(name,encrypt_button,mgs_label)])
        # decrypt_button = ttk.Button(root, text="Decryption", command=lambda: [reset_app()
        #                                                                       ,show_additional_buttons(name,mgs_label)
        #                                                                       ,message(name,decrypt_button,mgs_label)])
        # encrypt_button.place(relx=0.25, rely=0.40, anchor=CENTER)
        # decrypt_button.place(relx=0.7, rely=0.40, anchor=CENTER)

def open_folder():
    """
    This function opens the file dialog box for choosing a file.
    And then creates two buttons: encrypt_button, decrypt_button
    """
    username = getuser()
    initialdirectory = "C:/Users/{}".format(username)
    name = askopenfilename(
        initialdir=initialdirectory,
        filetypes=[("All Files", "*.*")],
        title="Choose a file."
    )
    if name:
        file_name = get_file_name(name, extension=True)
        label.config(text=file_name)
        mgs_label = ttk.Label(root)
        mgs_label.place(x=0, y=150)
        encrypt_button = ttk.Button(root, text="EncryptionDir", command=lambda: [reset_app()
                                                                              ,show_dir_buttons(name,mgs_label)
                                                                              ,message(name,encrypt_button,mgs_label)])
        decrypt_button = ttk.Button(root, text="DecryptionDir", command=lambda: [reset_app()
                                                                              ,show_dir_buttons(name,mgs_label)
                                                                              ,message(name,decrypt_button,mgs_label)])
        encrypt_button.place(relx=0.25, rely=0.40, anchor=CENTER)
        decrypt_button.place(relx=0.7, rely=0.40, anchor=CENTER)
        
def show_dir_buttons(name, mgs_label):
    reset_app()
    encrypt_button = ttk.Button(root, text="EncryptionDir", 
                                command=lambda: [reset_app(), message(name, encrypt_button, mgs_label)])
    decrypt_button = ttk.Button(root, text="DecryptionDir", 
                                command=lambda: [reset_app(), message(name, decrypt_button, mgs_label)])
    upload_button = ttk.Button(root, text="UploadDir", 
                                command=lambda: [reset_app(), message(name, upload_button, mgs_label)])
    download_button = ttk.Button(root, text="DownloadDir", 
                                command=lambda: [reset_app(), message(name, download_button, mgs_label)])
    schedule_button = ttk.Button(root, text="Schedule Upload Dir", 
                                command=lambda: [reset_app(), message(name, schedule_button, mgs_label)])
    encrypt_button.place(relx=0.25, rely=0.40, anchor=CENTER)
    decrypt_button.place(relx=0.7, rely=0.40, anchor=CENTER)
    upload_button.place(relx=0.25, rely=0.50, anchor=CENTER)
    schedule_button.place(relx=0.47, rely=0.5, anchor=CENTER)
    download_button.place(relx=0.7, rely=0.50, anchor=CENTER)

def show_additional_buttons(name, mgs_label):
    reset_app()
    upload_button = ttk.Button(root, text="Upload", 
                               command=lambda: message(name,upload_button,mgs_label))
    download_button = ttk.Button(root, text="Download",
                                command=lambda: message(name,download_button,mgs_label))
    # schedule_button = ttk.Button(root, text="Schedule Upload")
    # look_file_button = ttk.Button(root, text="Look File")

    upload_button.place(relx=0.25, rely=0.50, anchor=CENTER)
    download_button.place(relx=0.7, rely=0.50, anchor=CENTER)
    # schedule_button.place(relx=0.25, rely=0.65, anchor=CENTER)
    # look_file_button.place(relx=0.7, rely=0.65, anchor=CENTER)

def get_open_file():
    """
    This function opens the file dialog box for choosing a file.
    And then creates two buttons: encrypt_button, decrypt_button
    """
    username = getuser()
    initialdirectory = "C:/Users/{}".format(username)
    name = askopenfilename(
        initialdir=initialdirectory,
        filetypes=[("All Files", "*.*")],
        title="Choose a file."
    )
    if name:
        file_name = get_file_name(name, extension=True)
        label.config(text=file_name)
        mgs_label = ttk.Label(root)
        mgs_label.place(x=0, y=150)
        show_all_button(name,type='file',mgs_label=mgs_label)
    

def show_all_button(name,type,mgs_label):
    encrypt_button = ttk.Button(root, text="Encryption", 
                                command=lambda: [reset_app(), message(name, encrypt_button,type, mgs_label)])
    decrypt_button = ttk.Button(root, text="Decryption", 
                                command=lambda: [reset_app(), message(name, decrypt_button,type, mgs_label)])
    upload_button = ttk.Button(root, text="Upload", 
                                command=lambda: [reset_app(), message(name, upload_button,type, mgs_label)])
    download_button = ttk.Button(root, text="Download", 
                                command=lambda: [reset_app(), message(name, download_button,type, mgs_label)])
    # schedule_button = ttk.Button(root, text="Schedule Upload Dir", 
    #                             command=lambda: [reset_app(), message(name, schedule_button,type, mgs_label)])
    encrypt_button.place(relx=0.25, rely=0.40, anchor=CENTER)
    decrypt_button.place(relx=0.7, rely=0.40, anchor=CENTER)
    upload_button.place(relx=0.25, rely=0.50, anchor=CENTER)
    # schedule_button.place(relx=0.47, rely=0.5, anchor=CENTER)
    download_button.place(relx=0.7, rely=0.50, anchor=CENTER)
    upload_button.place(relx=0.25, rely=0.50, anchor=CENTER)
    download_button.place(relx=0.7, rely=0.50, anchor=CENTER)

   
def show_schedule_entry(name, mgs_label):
    reset_app()
    
    def submit_schedule():
        schedule_date = schedule_entry.get()
        try:
            schedule_datetime = datetime.strptime(schedule_date, "%Y-%m-%d")
        except ValueError:
            messagebox.showerror("Invalid Date", "Please enter a valid date in the format YYYY-MM-DD.")
            return
        schedule_window.destroy()
    schedule_window = Toplevel(root)
    schedule_window.title("Schedule Upload")

    schedule_label = ttk.Label(schedule_window, text="Enter date (YYYY-MM-DD):")
    schedule_label.pack()

    schedule_entry = ttk.Entry(schedule_window)
    schedule_entry.pack()

    schedule_button = ttk.Button(schedule_window, text="Submit", command=submit_schedule)
    schedule_button.pack()

    schedule_window.transient(root)  
    schedule_window.grab_set() 
    root.wait_window(schedule_window)





def reset_app():
    label.config(text="No chosen file")
    if separator2:
        separator2.destroy()
    if mgs_label:
        mgs_label.destroy()
    if encrypt_button:
        encrypt_button.destroy()
    if decrypt_button:
        decrypt_button.destroy()
    if upload_button:
        upload_button.destroy()
    if download_button:
        download_button.destroy()
    if schedule_button:
        schedule_button.destroy()
    if look_file_button:
        look_file_button.destroy()

def my_fun(): 
    path = askdirectory() # select directory 
    # l1.config(text=path) # update the text of Label with directory path
    root=next(os.walk(path))[0] # path 
    dirnames=next(os.walk(path))[1] # list of directories 
    files=next(os.walk(path))[2] # list of files 
    print(root) # D:\my_dir\my_dir0
    print(dirnames) # ['my_dir1']
    print(files) # ['my_file0.txt']
    for item in trv.get_children():
        trv.delete(item)
    i=1
    f2i=1 #sub directory id 
    for d in dirnames:
        trv.insert("", 'end',iid=i,values =d)
        path2=path+'/'+d # Path for sub directory 
        #print(path2)
        files2=next(os.walk(path2))[2] # file list of Sub directory 
        for f2 in files2:  # list of files 
            #print(f2)
            trv.insert(i, 'end',iid='sub'+str(f2i),values ="-" + f2)
            f2i=f2i+1
        i=i+1

    for f in files:  # list of files 
        trv.insert("", 'end',iid=i,values =f)
        i=i+1
        
    name = get_file_name(path)
    label.config(text=name)
    show_all_button(path,type='folder',mgs_label=mgs_label)

def my_fun_2(path): 
    #  = askdirectory() # select directory 
    # l1.config(text=path) # update the text of Label with directory path
    root=next(os.walk(path))[0] # path 
    dirnames=next(os.walk(path))[1] # list of directories 
    files=next(os.walk(path))[2] # list of files 
    print(root) # D:\my_dir\my_dir0
    print(dirnames) # ['my_dir1']
    print(files) # ['my_file0.txt']
    for item in trv.get_children():
        trv.delete(item)
    i=1
    f2i=1 #sub directory id 
    for d in dirnames:
        trv.insert("", 'end',iid=i,values =d)
        path2=path+'/'+d # Path for sub directory 
        #print(path2)
        files2=next(os.walk(path2))[2] # file list of Sub directory 
        for f2 in files2:  # list of files 
            #print(f2)
            trv.insert(i, 'end',iid='sub'+str(f2i),values ="-" + f2)
            f2i=f2i+1
        i=i+1

    for f in files:  # list of files 
        trv.insert("", 'end',iid=i,values =f)
        i=i+1
        
    name = get_file_name(path)
    label.config(text=name)


def tree_files_and_folders(files, folders, parent="", indent=1):
    
    for file in files:
        # print(' ' * indent + file['name'], "-", file['id'])
        trv2.insert(parent, 'end',text=file['name'],values=('-' * indent + file['name'],file['id'],file['mimeType']))
    
    for folder in folders:
        item = trv2.insert(parent, 'end',text=folder['name'],values=('+' * indent + folder['name'],folder['id'], file['mimeType']))
        subfiles, subfolders = list_files_and_folders(folder['id'])
        tree_files_and_folders(subfiles, subfolders, parent=item, indent=indent + 2)

def get_item_in_tree(event):
    selected_items = trv2.selection()
    for item in selected_items:
        item_values = trv2.item(item, 'values')  # Get the values of the selected item
        if item_values:
            item_id = item_values[1]  # Get the ID from the values text/plain
            item_type = item_values[2]
            global selection
            # global type_selection
            print("folder ID:", item_values[0])
            if item_type == "application/vnd.google-apps.folder": 
                selection=item_values
                print("folder ID:", item_values[0] + " - " + item_id," - type: " + item_type)
            else:
                selection=item_values
                print("file ID:",item_values[0] + " - " + item_id," - type: " + item_type)
        

root = tk.ThemedTk()
# root.configure(background="#FF0000")  # Thay đổi màu nền thành màu đỏ
root.get_themes()
root.set_theme("radiance")  # Set the theme to "radiance" for a modern look
icon = PhotoImage(file="images/icon.png")  # Icon for the window
root.iconphoto(False, icon)
app_width = 1000  # Window width
app_height = 550  # Window height
screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight()
# Calculate X and Y coordinates to center the window
x = int((screen_width / 2) - (app_width / 2))
y = int((screen_height / 2) - (app_height / 2))
root.geometry(f"{app_width}x{app_height}+{x}+{y}")
root.resizable(0, 0)  # Window size constant
root.title("Huyen Thuong Encryption")  # Set the window title

# Create a Frame for the content
content_frame = ttk.Frame(root, padding=20)
content_frame.pack(fill="both", expand=True)

title_label = ttk.Label(content_frame, text="Welcome to Dodo_ace Application", font=("Arial", 16, "bold"))
title_label.pack(pady=10)

file_frame = ttk.Frame(content_frame)
file_frame.pack(pady=20)

choose_file_button = ttk.Button(file_frame, text="Choose File", command=open_file)
choose_file_button.pack(side=LEFT)

choose_dir_button = ttk.Button(file_frame, text="Choose Directory"
                               , command=lambda: [my_fun()])
choose_dir_button.pack(side=RIGHT)

label = ttk.Label(file_frame, text="No chosen file", font=("Arial", 12))
label.pack(side=LEFT, padx=10)

separator1 = ttk.Separator(content_frame, orient='horizontal')
separator1.pack(fill="x", pady=20)

tree_frame = ttk.Frame(root)
tree_frame.pack(fill='both', padx=10, pady=5)

trv = ttk.Treeview(tree_frame, selectmode='browse', height=9)
trv.grid(row=1,column=0,columnspan=2,padx=10,pady=5)
# trv.pack(fill='both', expand=True)
trv["columns"]=("1")
trv['show']='tree headings'
trv.column("#0", width = 20, anchor ='c')
trv.column("1",width=300,anchor='w')
trv.heading("#0", text ="#")
trv.heading("1",text="selected folder",anchor='w')



trv2 = ttk.Treeview(tree_frame, selectmode='browse', height=9)
trv2.grid(row=1,column=3,columnspan=2,padx=10,pady=5)
# trv2.pack(fill='both', expand=True)
trv2["columns"]=("1")
trv2['show']='tree headings'
trv2.column("#0", width = 20, anchor ='c')
trv2.column("1",width=600,anchor='w')
trv2.heading("#0", text ="#")
trv2.heading("1",text="folder in backup driver",anchor='w')

folder_id = extract_folder_id()
files, folders = list_files_and_folders(folder_id)
tree_files_and_folders(files,folders)

trv2.bind("<<TreeviewSelect>>", get_item_in_tree)

show_all_button(name=None,type=None,mgs_label=None)

# text_frame = ttk.Frame(root)
# text_frame.pack(fill='both', padx=50, pady=10)

# log_text = Text(text_frame,width=30, height=10)
# log_text.pack(fill='both')


root.mainloop()
